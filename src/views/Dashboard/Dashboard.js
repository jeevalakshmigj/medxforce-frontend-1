import React, { Component } from "react";
import "./StyleSheets/Dashboard.css";
import ChartSection from "./Components/ChartSection";
import DashboardHeaderSection from "./Components/DashboardHeaderSection";
import OrderSection from "./Components/OrderSection";
import TodoList from "./Components/TodoList";
import ServiceRequests from "./Components/ServiceRequests";
import Revenue from "./Components/Revenue";
import ServiceProviders from "./Components/ServiceProviders";
import DashboardHeaderCard from "./Components/DashboardHeaderCard";
import DashboardService from "./Services/DashboardService";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: {},
    };
  }

  componentDidMount() {
    DashboardService.getUsers().then((response) => {
      this.setState({ users: response.data });
      console.log("res ", response);
    });
  }

  render() {
    return (
      <div className="main-content-container p-4 container-fluid">
        <div className="right-panel">
          <div className="column">
            <div className="row">
              <DashboardHeaderCard
                count1={"3"}
                title={"Registration Requests"}
                icon1={"1"}
                color1={"flat-color-1"}
                info1={"Due"}
                count2={"5"}
                info2={"Overdue"}
                color2={"flat-color-2"}
              />

              <DashboardHeaderCard
                count1={"3"}
                title={"Service Requests"}
                icon1={"1"}
                color1={"flat-color-1"}
                info1={"Due"}
                count2={"5"}
                info2={"Overdue"}
                color2={"flat-color-2"}
              />

              <TodoList />
            </div>
            <div className="row">
              <DashboardHeaderCard
                count1={"3"}
                title={"Issues"}
                icon1={"1"}
                color1={"flat-color-1"}
                info1={"Due"}
                count2={"5"}
                info2={"Overdue"}
                color2={"flat-color-2"}
              />

              <ServiceProviders
                color1={"flat-color-1"}
                info1={"Active"}
                count1={this.state.users.availTrue}
                color2={"flat-color-1"}
                info2={"Active"}
                count2={this.state.users.availFalse}
                color3={"flat-color-1"}
                info3={"Active"}
                count3={this.state.users.atWork}
              />

              <ServiceRequests
                color1={"flat-color-1"}
                info1={"Active"}
                count1={"15"}
                color2={"flat-color-1"}
                info2={"Active"}
                count2={"90"}
                color3={"flat-color-1"}
                info3={"Active"}
                count3={"32"}
                color4={"flat-color-1"}
                info4={"Active"}
                count4={"18"}
                color5={"flat-color-1"}
                info5={"Active"}
                count5={"2"}
              />
            </div>
          </div>

          <ChartSection />
          <Revenue />
        </div>
      </div>
    );
  }
}

export default Dashboard;
